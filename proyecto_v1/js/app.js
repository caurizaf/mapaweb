let map = L.map('map').setView([4.7388479, -74.1120093], 19);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([4.7388479, -74.1120093]).addTo(map)
    .bindPopup('Casa Carlos Uriza');

L.marker([4.6137759, -74.0638492]).addTo(map)
    .bindPopup('UD Macarena');